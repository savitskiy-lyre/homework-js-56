import React from 'react';
import './Ingredient.css';

const Ingredient = ({name, amount, decreaseAmount, increaseAmount, resetAmount}) => {
   return (
     <table className="table">
        <tbody>
        <tr>
           <th>
              <div className={"ingredient-img " + name} onClick={increaseAmount}/>
           </th>
           <th>{name}</th>
           <th>
              <div className="igredient-count">
                 <button onClick={decreaseAmount}>{'<'}</button>
                 <input type="text" value={'x' + amount} readOnly/>
                 <button onClick={increaseAmount}>{'>'}</button>
              </div>
           </th>
           <th>
              <button onClick={resetAmount}>X</button>
           </th>
        </tr>
        </tbody>
     </table>
   );
};

export default Ingredient;